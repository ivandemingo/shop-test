.PHONY: start-dev
start-dev:
	docker-compose -f docker-compose.dev.yml up

.PHONY: stop-dev
stop-dev:
	docker-compose -f docker-compose.dev.yml stop

.PHONY: down-dev
down-dev:
	docker-compose -f docker-compose.dev.yml down

.PHONY: start
start:
	docker-compose up

.PHONY: stop
stop:
	docker-compose stop

.PHONY: down
down:
	docker-compose down