# Shop Test

Shop Test is a demo project built with Spring Cloud microservices architecture and Angular.

## How to start the project

The components of this project are containerized with Docker Compose.

There are two environments which can be started with a different command:

* _Production like environment_: This environment uses small and simple images, which are ready to run. The application will be available at http://localhost:80

	```make start```

* _Development environment_: This environment can be used for development purposes. In this case, I have had to increase the Docker memory limit to 3 GB. The application will be available at http://localhost:4200

	```make start-dev```

The first build and initialization of the project will take a while. Every Spring project has to download the dependencies, build the JAR executable file and start the server.

Once all the servers are up, they have to register themselves in the Discovery Server. Don't worry if this operation fails, because it will be retried until the Discovery Server is up.

## Architecture

### Frontend:

The frontend of this project is implemented with TypeScript and using the Angular framework with the Material library.

* **WEB-APP**: Angular 9

### Backend:

The backend consists of a Spring Cloud architecture with multiple microservices written in Java 11.

* **API-GATEWAY**: Spring Cloud Gateway (Hoxton SR3).
* **DISCOVERY-SERVER**: Spring Cloud Netflix Eurerka discovery server (version 2.2.2)
* **CATALOG**: Spring Boot REST API (version 2.2.6)
* **CART**: Spring Boot REST API (version 2.2.6) - Hexagonal Architecture with Domain-Driven Design

Both REST APIs share the same relational database in MySQL 5.7.

![](docs/images/diagram.png)

## Releases

The development of this project has been done using a Scrum methodology in order to prioritize the value delivered to the user.

Git Flow is the tool that has helped managing the project development process and dividing it in different features and releases.

Every release and feature listed below delivers its own value to the user.

### 0.1

* List products
* View product details

### 0.2

* Create product
* Delete product
* Edit product

### 1.0

* Buy product
* List orders

## Screenshots

![](docs/images/screenshots.png)

## Attributions

The `box.png` icon used in the Product Details page is made by [*Freepik*](https://www.flaticon.com/authors/freepik) from www.flaticon.com.