package com.shoptest.cart.domain.product.service;

import com.shoptest.cart.domain.product.Product;

public class GetProductFakeService implements GetProductService {
    public static final int STOCK = 5;
    public static final float PRICE = 1.5f;

    @Override
    public Product get(int id) {
        return new Product(id, STOCK, PRICE);
    }
}
