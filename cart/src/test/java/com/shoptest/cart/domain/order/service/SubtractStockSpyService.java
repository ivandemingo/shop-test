package com.shoptest.cart.domain.order.service;

import com.shoptest.cart.domain.product.exception.ProductNotFoundException;
import com.shoptest.cart.domain.product.service.SubtractStockService;

public class SubtractStockSpyService implements SubtractStockService {
    private int productId;
    private int quantity;
    private int notFoundProductId;
    private int numberOfCalls = 0;

    public SubtractStockSpyService(int notFoundProductId) {
        this.notFoundProductId = notFoundProductId;
    }

    @Override
    public void subtract(int productId, int quantity) throws ProductNotFoundException {
        if (productId == this.notFoundProductId) {
            throw new ProductNotFoundException();
        }

        this.productId = productId;
        this.quantity = quantity;
        numberOfCalls++;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getNumberOfCalls() {
        return numberOfCalls;
    }
}
