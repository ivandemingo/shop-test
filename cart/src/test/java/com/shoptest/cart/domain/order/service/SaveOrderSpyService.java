package com.shoptest.cart.domain.order.service;

import com.shoptest.cart.domain.order.Order;

public class SaveOrderSpyService implements SaveOrderService {
    private Order savedOrder;
    private int numberOfCalls = 0;

    @Override
    public Order save(Order order) {
        savedOrder = order;
        numberOfCalls++;
        return order;
    }

    public Order getSavedOrder() {
        return savedOrder;
    }

    public int getNumberOfCalls() {
        return numberOfCalls;
    }
}
