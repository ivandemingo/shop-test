package com.shoptest.cart.application.order;

import com.shoptest.cart.application.order.exception.NotEnoughStockLeftException;
import com.shoptest.cart.domain.order.service.SaveOrderSpyService;
import com.shoptest.cart.domain.order.service.SubtractStockSpyService;
import com.shoptest.cart.domain.product.exception.ProductNotFoundException;
import com.shoptest.cart.domain.product.service.GetProductFakeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreateOrderServiceTest {
    private static final int NOT_FOUND_PRODUCT_ID = 2;

    private SaveOrderSpyService saveOrderService;
    private SubtractStockSpyService subtractStockService;
    private CreateOrderService service;

    @BeforeEach
    void setUp() {
        saveOrderService = new SaveOrderSpyService();
        subtractStockService = new SubtractStockSpyService(NOT_FOUND_PRODUCT_ID);
        service = new CreateOrderService(new GetProductFakeService(), saveOrderService, subtractStockService);
    }

    @Test()
    void shouldThrowExceptionWhenTheProductIsNotFound() {
        Assertions.assertThrows(
                ProductNotFoundException.class,
                () -> service.execute(new CreateOrderRequest(NOT_FOUND_PRODUCT_ID, 1))
        );
    }

    @Test
    void shouldThrowExceptionWhenNotEnoughStockLeft() {
        Assertions.assertThrows(
                NotEnoughStockLeftException.class,
                () -> service.execute(new CreateOrderRequest(1, GetProductFakeService.STOCK + 1))
        );
    }

    @Test
    void shouldSubtractStockWhenTheOrderIsSuccessfullyCreated() throws NotEnoughStockLeftException, ProductNotFoundException {
        CreateOrderResponse response = service.execute(new CreateOrderRequest(1, 2));

        Assertions.assertEquals(1, response.getOrder().getProductId());
        Assertions.assertEquals(2, response.getOrder().getQuantity());
        Assertions.assertEquals(1.5f * 2, response.getOrder().getTotalPrice());

        Assertions.assertEquals(1, saveOrderService.getNumberOfCalls());
        Assertions.assertEquals(1, subtractStockService.getNumberOfCalls());
        Assertions.assertEquals(1, subtractStockService.getProductId());
        Assertions.assertEquals(2, subtractStockService.getQuantity());
    }
}
