package com.shoptest.cart.infrastructure.controller;

import com.shoptest.cart.application.order.CreateOrderRequest;
import com.shoptest.cart.application.order.CreateOrderResponse;
import com.shoptest.cart.application.order.exception.NotEnoughStockLeftException;
import com.shoptest.cart.domain.order.Order;
import com.shoptest.cart.application.order.CreateOrderService;
import com.shoptest.cart.domain.product.exception.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateOrderController {

    @Autowired
    private CreateOrderService createOrderService;

    @PostMapping("/orders")
    public ResponseEntity<Order> create(@RequestBody CreateOrderRequest request)
            throws NotEnoughStockLeftException, ProductNotFoundException {
        CreateOrderResponse response = createOrderService.execute(request);

        return ResponseEntity
                .ok()
                .body(response.getOrder());
    }
}
