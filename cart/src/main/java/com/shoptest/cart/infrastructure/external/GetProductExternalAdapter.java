package com.shoptest.cart.infrastructure.external;

import com.shoptest.cart.domain.product.service.GetProductService;
import com.shoptest.cart.domain.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;

public class GetProductExternalAdapter implements GetProductService {

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Override
    public Product get(int id) {
        return webClientBuilder.build()
                .get()
                .uri("http://catalog-service/products/" + id)
                .retrieve()
                .bodyToMono(Product.class)
                .block();
    }
}
