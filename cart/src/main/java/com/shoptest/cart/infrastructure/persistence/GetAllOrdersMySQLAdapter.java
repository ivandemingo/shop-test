package com.shoptest.cart.infrastructure.persistence;

import com.shoptest.cart.domain.order.Order;
import com.shoptest.cart.domain.order.service.GetAllOrdersService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class GetAllOrdersMySQLAdapter implements GetAllOrdersService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> get() {
        return orderRepository.findAll();
    }
}
