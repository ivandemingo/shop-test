package com.shoptest.cart.infrastructure.external;

import com.shoptest.cart.domain.product.Product;
import com.shoptest.cart.domain.product.exception.ProductNotFoundException;
import com.shoptest.cart.domain.product.service.SubtractStockService;
import com.shoptest.cart.infrastructure.external.response.CatalogProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

public class SubtractStockExternalAdapter implements SubtractStockService {

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Override
    public void subtract(int productId, int quantity) throws ProductNotFoundException {
        CatalogProductResponse response = getCatalogProduct(productId);

        if (null == response) {
            throw new ProductNotFoundException();
        }

        response.setStock(response.getStock() - quantity);

        subtractStock(response);
    }

    private void subtractStock(CatalogProductResponse response) {
        webClientBuilder.build()
                .put()
                .uri("http://catalog-service/products")
                .body(BodyInserters.fromValue(response))
                .retrieve()
                .bodyToMono(Product.class)
                .block();
    }

    private CatalogProductResponse getCatalogProduct(int productId) {
        return webClientBuilder.build()
                .get()
                .uri("http://catalog-service/products/" + productId)
                .retrieve()
                .bodyToMono(CatalogProductResponse.class)
                .block();
    }
}
