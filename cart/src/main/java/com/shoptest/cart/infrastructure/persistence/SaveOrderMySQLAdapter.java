package com.shoptest.cart.infrastructure.persistence;

import com.shoptest.cart.domain.order.Order;
import com.shoptest.cart.domain.order.service.SaveOrderService;
import org.springframework.beans.factory.annotation.Autowired;

public class SaveOrderMySQLAdapter implements SaveOrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }
}
