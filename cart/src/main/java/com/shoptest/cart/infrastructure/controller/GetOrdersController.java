package com.shoptest.cart.infrastructure.controller;

import com.shoptest.cart.application.order.GetOrderResponse;
import com.shoptest.cart.application.order.GetOrdersService;
import com.shoptest.cart.domain.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetOrdersController {

    @Autowired
    private GetOrdersService getOrdersService;

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> get() {
        GetOrderResponse response = getOrdersService.execute();

        return ResponseEntity
                .ok()
                .body(response.getOrders());
    }
}
