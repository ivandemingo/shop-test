package com.shoptest.cart;

import com.shoptest.cart.application.order.GetOrdersService;
import com.shoptest.cart.domain.order.service.GetAllOrdersService;
import com.shoptest.cart.domain.order.service.SaveOrderService;
import com.shoptest.cart.domain.product.service.SubtractStockService;
import com.shoptest.cart.infrastructure.external.GetProductExternalAdapter;
import com.shoptest.cart.application.order.CreateOrderService;
import com.shoptest.cart.domain.product.service.GetProductService;
import com.shoptest.cart.infrastructure.external.SubtractStockExternalAdapter;
import com.shoptest.cart.infrastructure.persistence.GetAllOrdersMySQLAdapter;
import com.shoptest.cart.infrastructure.persistence.SaveOrderMySQLAdapter;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class CartConfiguration {

    @Bean
    @LoadBalanced
    public WebClient.Builder weClientBuilder() {
        return WebClient.builder();
    }

    @Bean
    public GetProductService getProductExternalService() {
        return new GetProductExternalAdapter();
    }

    @Bean
    public SaveOrderService saveOrderMySQLService() {
        return new SaveOrderMySQLAdapter();
    }

    @Bean
    public SubtractStockService subtractStockExternalService() {
        return new SubtractStockExternalAdapter();
    }

    @Bean
    public GetAllOrdersService getAllOrderMySQLService() {
        return new GetAllOrdersMySQLAdapter();
    }

    @Bean
    public CreateOrderService getCreateOrderService() {
        return new CreateOrderService(
                getProductExternalService(),
                saveOrderMySQLService(),
                subtractStockExternalService()
        );
    }

    @Bean
    public GetOrdersService getGetOrderService() {
        return new GetOrdersService(getAllOrderMySQLService());
    }
}
