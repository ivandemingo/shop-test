package com.shoptest.cart.application.order;

import com.shoptest.cart.domain.order.Order;

import java.util.List;

public class GetOrderResponse {
    private List<Order> orders;

    public GetOrderResponse(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }
}
