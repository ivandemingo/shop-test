package com.shoptest.cart.application.order.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Not enough stock left")
public class NotEnoughStockLeftException extends Exception {
}
