package com.shoptest.cart.application.order;

import com.shoptest.cart.application.order.exception.NotEnoughStockLeftException;
import com.shoptest.cart.domain.order.Order;
import com.shoptest.cart.domain.product.exception.ProductNotFoundException;
import com.shoptest.cart.domain.product.service.GetProductService;
import com.shoptest.cart.domain.order.service.SaveOrderService;
import com.shoptest.cart.domain.product.Product;
import com.shoptest.cart.domain.product.service.SubtractStockService;

public class CreateOrderService {

    private GetProductService getProductService;
    private SaveOrderService saveOrderService;
    private SubtractStockService subtractStockService;

    public CreateOrderService(
            GetProductService getProductService,
            SaveOrderService saveOrderService,
            SubtractStockService subtractStockService
    ) {
        this.getProductService = getProductService;
        this.saveOrderService = saveOrderService;
        this.subtractStockService = subtractStockService;
    }

    public CreateOrderResponse execute(CreateOrderRequest request)
            throws NotEnoughStockLeftException, ProductNotFoundException {
        Product product = getProductService.get(request.getProductId());

        if (product.getStock() < request.getQuantity()) {
            throw new NotEnoughStockLeftException();
        }

        Order order = new Order(
                request.getProductId(), request.getQuantity(), product.getPrice() * request.getQuantity()
        );

        subtractStockService.subtract(request.getProductId(), request.getQuantity());

        return new CreateOrderResponse(saveOrderService.save(order));
    }
}
