package com.shoptest.cart.application.order;

import com.shoptest.cart.domain.order.service.GetAllOrdersService;

public class GetOrdersService {
    private GetAllOrdersService service;

    public GetOrdersService(GetAllOrdersService service) {
        this.service = service;
    }

    public GetOrderResponse execute() {
        return new GetOrderResponse(service.get());
    }
}
