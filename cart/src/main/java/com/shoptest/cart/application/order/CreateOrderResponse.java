package com.shoptest.cart.application.order;

import com.shoptest.cart.domain.order.Order;

public class CreateOrderResponse {

    private Order order;

    public CreateOrderResponse(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}
