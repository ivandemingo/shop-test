package com.shoptest.cart.application.order;

public class CreateOrderRequest {

    private int productId;
    private int quantity;

    public CreateOrderRequest(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
}
