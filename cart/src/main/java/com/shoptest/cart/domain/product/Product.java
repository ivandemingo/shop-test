package com.shoptest.cart.domain.product;

public class Product {

    private int id;
    private int stock;
    private float price;

    public Product() {
    }

    public Product(int id, int stock, float price) {
        this.id = id;
        this.stock = stock;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public int getStock() {
        return stock;
    }

    public float getPrice() {
        return price;
    }
}
