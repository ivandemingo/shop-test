package com.shoptest.cart.domain.order.service;

import com.shoptest.cart.domain.order.Order;

public interface SaveOrderService {
    Order save(Order order);
}
