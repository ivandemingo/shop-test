package com.shoptest.cart.domain.product.service;

import com.shoptest.cart.domain.product.exception.ProductNotFoundException;

public interface SubtractStockService {
    void subtract(int productId, int quantity) throws ProductNotFoundException;
}
