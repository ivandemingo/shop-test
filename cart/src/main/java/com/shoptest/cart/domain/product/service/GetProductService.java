package com.shoptest.cart.domain.product.service;

import com.shoptest.cart.domain.product.Product;

public interface GetProductService {
    Product get(int id);
}
