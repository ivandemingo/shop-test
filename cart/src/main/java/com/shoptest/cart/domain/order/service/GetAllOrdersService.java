package com.shoptest.cart.domain.order.service;

import com.shoptest.cart.domain.order.Order;

import java.util.List;

public interface GetAllOrdersService {
    List<Order> get();
}
