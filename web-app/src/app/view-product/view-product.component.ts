import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from '../products.service';
import {Product} from '../list-products/list-products.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {OrdersService} from '../orders.service';
import {Order} from '../list-orders/list-orders.component';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {
  product: Product;
  buyProductForm;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private ordersService: OrdersService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.buyProductForm = this.formBuilder.group({
      quantity: 0
    });
  }

  ngOnInit(): void {
    this.productsService
      .viewProduct(+this.route.snapshot.paramMap.get('productId'))
      .subscribe(
        (product: Product) => this.product = { ...product },
        error => this.openSnackbar(error)
      );
  }

  deleteProduct() {
    this.productsService.deleteProduct(this.product.id)
      .subscribe(
        () => {
          this.router
            .navigate(['/'])
            .then(() => this.openSnackbar('Product deleted successfully!'));
        },
        error => this.openSnackbar(error)
      );
  }

  openSnackbar(error: string) {
    this.snackBar.open(error, 'CLOSE', { duration: 3000 });
  }

  onSubmit(buyProductData) {
    const quantity = buyProductData.quantity;

    if (quantity < 1) {
      this.openSnackbar('Quantity must be greater than 0');
      return;
    }

    this.ordersService.createOrder(this.product.id, quantity)
      .subscribe(
        (order: Order) => {
          this.router.navigate(['/orders'])
            .then(() => this.openSnackbar('Order with id: ' + order.id + ' successfully created!'));
        },
        (error) => this.openSnackbar(error)
      );
  }
}
