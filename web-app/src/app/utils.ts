export const isNullOrEmpty = arg => null === arg || '' === arg;
