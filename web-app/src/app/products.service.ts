import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Product} from './list-products/list-products.component';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private http: HttpClient
  ) { }

  listProducts() {
    return this.http.get<Product[]>('http://localhost:8080/products/')
      .pipe(catchError(error => this.handleError(error)));
  }

  viewProduct(id: number) {
    return this.http.get<Product>('http://localhost:8080/products/' + id)
      .pipe(catchError(error => this.handleError(error)));
  }

  createProduct(name: string, stock: number, price: number) {
    return this.http.put<Product>('http://localhost:8080/products/', { name, stock, price })
      .pipe(catchError(error => this.handleError(error)));
  }

  deleteProduct(id: number) {
    return this.http.delete('http://localhost:8080/products/' + id)
      .pipe(catchError(error => this.handleError(error)));
  }

  editProduct(id: number, name: string, stock: number, price: number) {
    return this.http.put<Product>('http://localhost:8080/products/', { id, name, stock, price })
      .pipe(catchError(error => this.handleError(error)));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return throwError('Network error. Please try again later.');
    }

    if (!error.error.message) {
      return throwError('An error occurred. Please try again later.');
    }

    return throwError(error.error.message);
  }

}
