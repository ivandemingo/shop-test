import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Order} from './list-orders/list-orders.component';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(
    private http: HttpClient
  ) { }

  createOrder(productId: number, quantity: number) {
    return this.http.post('http://localhost:8080/orders/', { productId, quantity })
      .pipe(catchError(error => this.handleError(error)));
  }

  listOrders() {
    return this.http.get<Order[]>('http://localhost:8080/orders/')
      .pipe(catchError(error => this.handleError(error)));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return throwError('Network error. Please try again later.');
    }

    if (!error.error.message) {
      return throwError('An error occurred. Please try again later.');
    }

    return throwError(error.error.message);
  }
}
