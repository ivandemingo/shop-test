import { Component, OnInit } from '@angular/core';
import {Product} from '../list-products/list-products.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProductsService} from '../products.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {isNullOrEmpty} from '../utils';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product: Product;
  editProductForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.editProductForm = this.formBuilder.group({
      name: '',
      stock: '',
      price: ''
    });
  }

  ngOnInit(): void {
    this.productsService
      .viewProduct(+this.route.snapshot.paramMap.get('productId'))
      .subscribe(
        (product: Product) => {
          this.product = product;
          this.editProductForm.controls.name.setValue(product.name);
          this.editProductForm.controls.stock.setValue(product.stock);
          this.editProductForm.controls.price.setValue(product.price);
        },
        error => this.openSnackbar(error)
      );
  }

  onSubmit(productData) {
    if (isNullOrEmpty(productData.name)
      || isNullOrEmpty(productData.stock)
      || isNullOrEmpty(productData.price)) {
      this.openSnackbar('Please fill in all the inputs');
      return;
    }

    this.productsService
      .editProduct(this.product.id, productData.name, productData.stock, productData.price)
      .subscribe(
        (product: Product) => {
          this.router.navigate(['/products/' + product.id])
            .then(() => this.openSnackbar('Product saved successfully!'));
        },
        error => this.openSnackbar(error)
      );
  }

  openSnackbar(error: string) {
    this.snackBar.open(error, 'CLOSE', { duration: 3000 });
  }

}
