import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ProductsService} from '../products.service';
import {Product} from '../list-products/list-products.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {isNullOrEmpty} from '../utils';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  createProductForm;

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.createProductForm = this.formBuilder.group({
      name: '',
      stock: '',
      price: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(productData) {
    if (isNullOrEmpty(productData.name)
      || isNullOrEmpty(productData.stock)
      || isNullOrEmpty(productData.price)) {
      this.openSnackbar('Please fill in all the inputs');
      return;
    }

    this.createProductForm.reset();
    this.productsService
      .createProduct(productData.name, productData.stock, productData.price)
      .subscribe(
        (product: Product) => {
          this.router
            .navigate(['/products/' + product.id])
            .then(() => this.openSnackbar('Product created successfully!'));
        },
        error => this.openSnackbar(error)
      );
  }

  private openSnackbar(error: string) {
    this.snackBar.open(error, 'CLOSE', { duration: 3000 });
  }

}
