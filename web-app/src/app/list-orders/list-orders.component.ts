import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Product} from '../list-products/list-products.component';
import {OrdersService} from '../orders.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';

export interface Order {
  id: number;
  productId: number;
  quantity: number;
  totalPrice: number;
}

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'productId', 'quantity', 'totalPrice'];
  dataSource: MatTableDataSource<Order> = new MatTableDataSource<Order>([]);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private ordersService: OrdersService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.ordersService.listOrders()
      .subscribe(
        (orders: Order[]) => {
          this.dataSource = new MatTableDataSource<Order>(orders.map(
            (order: Order) => ({ ...order })
          ));
          this.dataSource.paginator = this.paginator;
        },
        error => this.openSnackbar(error)
      );
  }

  openSnackbar(error: string) {
    this.snackBar.open(error, 'CLOSE', { duration: 3000 });
  }

}
