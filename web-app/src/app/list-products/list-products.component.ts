import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductsService} from '../products.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';

export interface Product {
  id: number;
  name: string;
  stock: number;
  price: number;
}

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css'],
})
export class ListProductsComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'stock', 'price', 'actions'];
  dataSource: MatTableDataSource<Product> = new MatTableDataSource<Product>([]);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private productsService: ProductsService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.productsService.listProducts()
      .subscribe(
        (products: Product[]) => {
          this.dataSource = new MatTableDataSource<Product>(products.map(
            (product: Product) => ({ ...product })
          ));
          this.dataSource.paginator = this.paginator;
        },
        error => this.openSnackbar(error)
      );
  }

  openSnackbar(error: string) {
    this.snackBar.open(error, 'CLOSE', { duration: 3000 });
  }

  deleteProduct(id: number) {
    this.productsService.deleteProduct(id)
      .subscribe(
        () => {
          window.location.reload();
          this.openSnackbar('Product deleted successfully!');
        },
        error => this.openSnackbar(error)
      );
  }

}
