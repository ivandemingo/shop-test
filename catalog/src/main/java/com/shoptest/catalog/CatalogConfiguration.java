package com.shoptest.catalog;

import com.shoptest.catalog.product.service.SaveProductService;
import com.shoptest.catalog.product.service.DeleteProductService;
import com.shoptest.catalog.product.service.ListProductsService;
import com.shoptest.catalog.product.service.ViewProductService;
import com.shoptest.catalog.product.service.adapter.SaveProductMySQLAdapter;
import com.shoptest.catalog.product.service.adapter.DeleteProductMySQLAdapter;
import com.shoptest.catalog.product.service.adapter.ListProductsMySQLAdapter;
import com.shoptest.catalog.product.service.adapter.ViewProductMySQLAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CatalogConfiguration {

    @Bean
    public ListProductsService listProductsMySQLService() {
        return new ListProductsMySQLAdapter();
    }

    @Bean
    public ViewProductService viewProductMySQLService() {
        return new ViewProductMySQLAdapter();
    }

    @Bean
    public SaveProductService saveProductMySQLService() {
        return new SaveProductMySQLAdapter();
    }

    @Bean
    public DeleteProductService deleteProductMySQLService() {
        return new DeleteProductMySQLAdapter();
    }
}
