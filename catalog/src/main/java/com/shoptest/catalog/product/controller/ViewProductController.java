package com.shoptest.catalog.product.controller;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.exception.ProductNotFoundException;
import com.shoptest.catalog.product.service.ViewProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViewProductController {

    @Autowired
    @Qualifier("viewProductMySQLService")
    private ViewProductService viewProductService;

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> view(@PathVariable String id) throws ProductNotFoundException {
        Product product = viewProductService.view(Integer.parseInt(id));

        return ResponseEntity
                .ok()
                .body(product);
    }
}
