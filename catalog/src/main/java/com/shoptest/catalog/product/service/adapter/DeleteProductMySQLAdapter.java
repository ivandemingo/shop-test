package com.shoptest.catalog.product.service.adapter;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.exception.ProductNotFoundException;
import com.shoptest.catalog.product.persistence.ProductRepository;
import com.shoptest.catalog.product.service.DeleteProductService;
import com.shoptest.catalog.product.service.ViewProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class DeleteProductMySQLAdapter implements DeleteProductService {
    @Autowired
    private ProductRepository repository;

    @Autowired
    @Qualifier("viewProductMySQLService")
    private ViewProductService viewProductService;

    @Override
    public void delete(int id) throws ProductNotFoundException {
        Product product = viewProductService.view(id);

        repository.delete(product);
    }
}
