package com.shoptest.catalog.product.service;

import com.shoptest.catalog.product.Product;

import java.util.List;

public interface ListProductsService {
    List<Product> list();
}
