package com.shoptest.catalog.product.controller;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.service.SaveProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaveProductController {

    @Autowired
    @Qualifier("saveProductMySQLService")
    private SaveProductService saveProductService;

    @PutMapping("/products")
    public ResponseEntity<Product> save(@RequestBody Product product) {
        Product createdProduct = saveProductService.save(product);

        return ResponseEntity
                .ok()
                .body(createdProduct);
    }
}
