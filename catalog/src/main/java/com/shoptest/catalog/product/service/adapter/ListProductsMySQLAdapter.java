package com.shoptest.catalog.product.service.adapter;

import com.shoptest.catalog.product.service.ListProductsService;
import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.persistence.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ListProductsMySQLAdapter implements ListProductsService {
    @Autowired
    private ProductRepository repository;

    @Override
    public List<Product> list() {
        return repository.findAll();
    }
}
