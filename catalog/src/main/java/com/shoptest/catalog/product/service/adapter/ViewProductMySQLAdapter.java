package com.shoptest.catalog.product.service.adapter;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.exception.ProductNotFoundException;
import com.shoptest.catalog.product.persistence.ProductRepository;
import com.shoptest.catalog.product.service.ViewProductService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class ViewProductMySQLAdapter implements ViewProductService {
    @Autowired
    private ProductRepository repository;

    @Override
    public Product view(int id) throws ProductNotFoundException {
        Optional<Product> product = repository.findById(id);

        if (product.isEmpty()) {
            throw new ProductNotFoundException();
        }

        return product.get();
    }
}
