package com.shoptest.catalog.product.service;

import com.shoptest.catalog.product.exception.ProductNotFoundException;

public interface DeleteProductService {
    void delete(int id) throws ProductNotFoundException;
}
