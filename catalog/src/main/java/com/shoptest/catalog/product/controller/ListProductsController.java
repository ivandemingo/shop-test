package com.shoptest.catalog.product.controller;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.service.ListProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListProductsController {

    @Autowired
    @Qualifier("listProductsMySQLService")
    private ListProductsService listProductsService;

    @GetMapping("/products")
    public ResponseEntity<List<Product>> list() {
        List<Product> products = listProductsService.list();

        return ResponseEntity
                .ok()
                .body(products);
    }
}
