package com.shoptest.catalog.product.controller;

import com.shoptest.catalog.product.exception.ProductNotFoundException;
import com.shoptest.catalog.product.service.DeleteProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeleteProductController {

    @Autowired
    @Qualifier("deleteProductMySQLService")
    private DeleteProductService deleteProductService;

    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) throws ProductNotFoundException {
        deleteProductService.delete(Integer.parseInt(id));

        return ResponseEntity
                .ok()
                .build();
    }
}
