package com.shoptest.catalog.product.service;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.exception.ProductNotFoundException;

public interface ViewProductService {
    Product view(int id) throws ProductNotFoundException;
}
