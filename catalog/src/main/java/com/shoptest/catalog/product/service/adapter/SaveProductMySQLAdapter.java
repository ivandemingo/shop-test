package com.shoptest.catalog.product.service.adapter;

import com.shoptest.catalog.product.Product;
import com.shoptest.catalog.product.persistence.ProductRepository;
import com.shoptest.catalog.product.service.SaveProductService;
import org.springframework.beans.factory.annotation.Autowired;

public class SaveProductMySQLAdapter implements SaveProductService {
    @Autowired
    private ProductRepository repository;

    @Override
    public Product save(Product product) {
        return repository.save(product);
    }
}
