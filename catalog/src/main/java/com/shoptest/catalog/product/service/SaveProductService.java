package com.shoptest.catalog.product.service;

import com.shoptest.catalog.product.Product;

public interface SaveProductService {
    Product save(Product product);
}
